import { Card, CurrencyEnum, Transaction, ICard } from "./index";
import { Pocket } from "./services/poket";

describe("class Card`s methods tests", () => {
  let myCard: Card;
  let trans1: Transaction;
  let trans2: Transaction;
  let myPocket: Pocket;

  beforeEach(() => {
    myCard = new Card();
    myPocket = new Pocket();

    trans1 = new Transaction(100, CurrencyEnum.USD);
    trans2 = new Transaction(100, CurrencyEnum.USD);

    myCard.addTransaction(trans1);
    myCard.addTransaction(trans2);

    myPocket.addCard("Vadym", myCard);
  });

  test("getTransaction", () => {
    expect(myCard.getTransaction(trans1.id)).toEqual(trans1);
    expect(myCard.getTransaction(trans2.id)).toEqual(trans2);
  });

  test("getBalance", () => {
    expect(myCard.getBalance(CurrencyEnum.UAH)).toBe(0);
    expect(myCard.getBalance(CurrencyEnum.USD)).toBe(200);
  });

  test("Add and get card", () => {
    expect(myPocket.getCard("Vadym")).toBe(myCard);
  });

  test("Remove card", () => {
    myPocket.removeCard("Vadym");
    expect(myPocket.getCard("Vadym")).toBeUndefined();
  });

  test("Get total amount in USD", () => {
    expect(myPocket.getTotalAmount(CurrencyEnum.USD)).toBe(200);
  });

  test("Get total amount in UAH", () => {
    expect(myPocket.getTotalAmount(CurrencyEnum.UAH)).toBe(0);
  });
});

import { v4 as uuidv4 } from "uuid";
import { Pocket } from "./services/poket";

export enum CurrencyEnum {
  USD = "USD",
  UAH = "UAH",
}

export interface ICard {
  addTransaction(arg1: Transaction | CurrencyEnum, arg2?: number): string;
  getTransaction(id: string): Transaction | undefined;
  getBalance(currency: CurrencyEnum): number;
}

export class Transaction {
  id: string;
  amount: number;
  currency: CurrencyEnum;

  constructor(amount: number, currency: CurrencyEnum) {
    this.id = uuidv4();
    this.amount = amount;
    this.currency = currency;
  }
}

export class Card implements ICard {
  transactions: Transaction[];

  constructor() {
    this.transactions = [];
  }

  addTransaction(transaction: Transaction): string;
  addTransaction(currency: CurrencyEnum, amount: number): string;
  addTransaction(arg1: Transaction | CurrencyEnum, arg2?: number): string {
    let transaction: Transaction;

    if (arg1 instanceof Transaction) {
      transaction = arg1;
    } else if (
      Object.values(CurrencyEnum).includes(arg1) &&
      arg2 !== undefined
    ) {
      transaction = new Transaction(arg2, arg1);
    } else {
      throw new Error("Invalid arguments");
    }

    this.transactions.push(transaction);

    return transaction.id;
  }

  getTransaction(id: string): Transaction | undefined {
    return this.transactions.find((transaction) => transaction.id === id);
  }

  getBalance(currency: CurrencyEnum): number {
    return this.transactions
      .filter((transaction) => transaction.currency === currency)
      .reduce((sum, transaction) => sum + transaction.amount, 0);
  }
}

export class BonusCard extends Card {
  transactions: Transaction[];

  constructor() {
    super();
    this.transactions = [];
  }

  addTransaction(transaction: Transaction): string;
  addTransaction(currency: CurrencyEnum, amount: number): string;
  addTransaction(arg1: Transaction | CurrencyEnum, arg2?: number): string {
    let transaction: Transaction;

    if (arg1 instanceof Transaction) {
      transaction = arg1;
    } else if (typeof arg1 === "number" && arg2 !== undefined) {
      transaction = new Transaction(arg2, arg1);
    } else {
      throw new Error("Invalid arguments");
    }

    this.transactions.push(transaction);

    const bonusAmount = transaction.amount * 0.1;
    const bonusTransaction = new Transaction(bonusAmount, transaction.currency);
    this.transactions.push(bonusTransaction);

    return transaction.id;
  }
}

import { ICard, CurrencyEnum } from "..";

export class Pocket {
  private cards: ICard[];

  constructor() {
    this.cards = [];
  }

  addCard(name: string, card: ICard): void {
    this.cards[name] = card;
  }

  removeCard(name: string): void {
    delete this.cards[name];
  }

  getCard(name: string): ICard | undefined {
    return this.cards[name];
  }

  getTotalAmount(currency: CurrencyEnum): number {
    let total = 0;
    for (const cardName in this.cards) {
      if (this.cards.hasOwnProperty.call(this.cards, cardName)) {
        total += this.cards[cardName].getBalance(currency);
      }
    }
    return total;
  }
}
